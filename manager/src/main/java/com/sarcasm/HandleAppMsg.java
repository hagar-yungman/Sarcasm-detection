package com.sarcasm;

import com.amazonaws.AmazonClientException;
import com.amazonaws.regions.Region;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.*;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class HandleAppMsg implements Runnable {
    private final static String APP_MANAGER_QUEUE_NAME = "app-manager";
    private static String APP_MANAGER_QUEUE_URL;
    private static AmazonSQS SQS = AmazonSQSClientBuilder.standard()
            .withRegion(managerApp.REGION)
            .build();
    private static AmazonEC2 EC2 = AmazonEC2ClientBuilder.standard()
            .withRegion(managerApp.REGION)
            .build();
    private static AmazonS3 S3 = AmazonS3ClientBuilder.standard()
            .withRegion(managerApp.REGION)
            .build();
    //fields
    private static String managerWorkersSqsUrl;
    private static ConcurrentHashMap<String, Integer> numOfReviewsOfFile;
    private static ConcurrentHashMap<String, String> urlsMap;
    private static AtomicBoolean terminate;
    //vars
    private static String bucketName;
    private static String inputKeyName;
    private static String outputKeyName;
    private static String dwonloadedFileName;
    private static int n;
    private static boolean isTerminate;
    private static String workerImage;


    //Constractor
    public HandleAppMsg(String managerWorkersSqsUrl, ReviewsMap reviewsMap, String workerImage) {
        this.managerWorkersSqsUrl = managerWorkersSqsUrl;
        this.numOfReviewsOfFile = reviewsMap.getNumOfReviewsPerFile();
        this.urlsMap = reviewsMap.getFilesUrls();
        this.terminate = reviewsMap.terminate;
        this.workerImage = workerImage;
        APP_MANAGER_QUEUE_URL = getAppManagerSqsUrl();
    }

    public void run() {
        //forever download a msg from the queue
        while (true) {
            System.out.println("waiting for massage from app");
            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(APP_MANAGER_QUEUE_URL);
            List<Message> messages = SQS.receiveMessage(receiveMessageRequest).getMessages();
            for (Message message : messages) {
                ObjectMapper mapper = new ObjectMapper();
                MsgFromApp msgFromApplication = null;
                try {
                    msgFromApplication = mapper.readValue(message.getBody(), MsgFromApp.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                bucketName = msgFromApplication.getBucketName();
                inputKeyName = msgFromApplication.getInputKeyName();
                outputKeyName = msgFromApplication.getOutputKeyName();
                dwonloadedFileName = msgFromApplication.getFileName();
                System.out.println("received message from app-manager with file: " + dwonloadedFileName);
                n = msgFromApplication.getN();
                isTerminate = msgFromApplication.isTerminate();
                urlsMap.put(msgFromApplication.getFileName(), msgFromApplication.getAppSqsUrl());
                if (isTerminate) {
                    System.out.println("received TERMINATE message!");
                    terminate.set(true);
                    System.out.println("delete msg from app manager");
                    SQS.deleteMessage(getAppManagerSqsUrl(), message.getReceiptHandle());
                    return;
                } else {
                    try {
                        parseFile(downloadFileFromS3(dwonloadedFileName).getObjectContent());
                        System.out.println("parse file");
                        //delete msg from queue
                        System.out.println("delete msg from app manager");
                        SQS.deleteMessage(getAppManagerSqsUrl(), message.getReceiptHandle());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }


    private static String getAppManagerSqsUrl() {
        try {
            String result = SQS.getQueueUrl(APP_MANAGER_QUEUE_NAME).getQueueUrl();
            System.out.println("get sqs: "+ APP_MANAGER_QUEUE_NAME + "url -------------------------------" +result);
            return result;
        }catch (com.amazonaws.services.sqs.model.QueueDoesNotExistException e) {
            System.out.println("queue was not found");
            return null;
        }

    }


    //get number of msg in a given queue
    private static int verifyNumberOfMessages(String queueUrl) {
        GetQueueAttributesRequest request = new GetQueueAttributesRequest();
        request = request.withAttributeNames("ApproximateNumberOfMessages");
        request = request.withQueueUrl(queueUrl);
        Map<String, String> attrs = SQS.getQueueAttributes(request).getAttributes();
        return Integer.parseInt(attrs.get("ApproximateNumberOfMessages"));
    }

    //parse each line of the file into a java class. crate messages
    //from the reviews
    private static void parseFile(InputStream text) throws Exception {
        int numOfReviewInFile = 0;
        BufferedReader br = new BufferedReader(
                new InputStreamReader(text));
        String line;
        while ((line = br.readLine()) != null) {
            ObjectMapper mapper = new ObjectMapper();
            Reviews reviews = mapper.readValue(line, Reviews.class);
            for (Review review : reviews.getReviews()) {
                review.setBucketName(bucketName);
                review.setOutputKeyName(outputKeyName);
                review.setFileName(dwonloadedFileName);
                review.setProductTitle(reviews.getTitle());
                //add line as message
                addMsgToSqs(mapper.writeValueAsString(review), managerWorkersSqsUrl);
                numOfReviewInFile++;
            }
        }
        numOfReviewsOfFile.put(dwonloadedFileName, new Integer(numOfReviewInFile));
        createWorkers();
        br.close();
    }

    //calc how many workers to initiate
    private static void createWorkers() {
        int numOfMsgs = verifyNumberOfMessages(managerWorkersSqsUrl);
        int numOfWorkers = getNumOfWorkersInstances();
        int numOfWorkersToCreate = (int)Math.ceil((double)numOfMsgs / n) - numOfWorkers;
        System.out.println("there are " + numOfMsgs + "reviews, and " +
                numOfWorkers + "workers, tring to create " +
                numOfWorkersToCreate + "workers");
            createNewInstance(numOfWorkersToCreate);

    }

    //create a msg request and add it to a SQS queue, if a queue does not exists, crates one
    private static void addMsgToSqs(String massage, String url) {
        SQS.sendMessage(new SendMessageRequest()
                .withQueueUrl(url)
                .withMessageBody(massage));
    }

    //download a file from S3
    private static S3Object downloadFileFromS3(String fileName) {
        S3Object result = null;
        try {
            result = S3.getObject(new GetObjectRequest(bucketName, inputKeyName + fileName));
            System.out.println("Downloading an object");
        } catch (AmazonClientException e) {
            e.printStackTrace();
        }
        return result;
    }

    private static int getNumOfWorkersInstances() {
        DescribeInstancesRequest request = new DescribeInstancesRequest();
        List<String> tagValue = new ArrayList<>();
        tagValue.add("worker");
        DescribeInstancesResult response = EC2.describeInstances(request.withFilters(new Filter().withName("tag:serverType").withValues(tagValue)));
        int numberOfWorkers = 0;
        for (Reservation reservation : response.getReservations()) {
            for (Instance instance : reservation.getInstances()) {
                if (instance.getState().getName().equals((String) "running")) {
                    numberOfWorkers++;
                }
            }
        }
        System.out.println("number of working workers" + numberOfWorkers);
        return numberOfWorkers;
    }

    private static void createNewInstance(int numToCreate) {
        String base64UserData = "";
        String userData = "#!/bin/bash\n" +
                "set -x \n" +
                "java -jar /home/ec2-user/worker.jar \n";
        try {
            base64UserData = new String(Base64.encodeBase64(userData.getBytes("UTF-8")), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        TagSpecification tagSpecification = new TagSpecification().withResourceType("instance")
                .withTags(new Tag("serverType", "worker"));
        RunInstancesRequest run_request = new RunInstancesRequest()
                .withImageId(workerImage).withUserData(base64UserData)
                .withInstanceType(InstanceType.T2Medium)
                .withMaxCount(numToCreate)
                .withMinCount(1)
                .withTagSpecifications(tagSpecification)
                .withIamInstanceProfile(new IamInstanceProfileSpecification().withName("WorkerRole"));
        EC2.runInstances(run_request);
        System.out.println("create instance");
    }
}
