package com.sarcasm;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.*;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class managerApp {
    public final static Regions REGION = Regions.US_EAST_1;

    private static AmazonSQS SQS = AmazonSQSClientBuilder.standard()
            .withRegion(REGION)
            .build();
    private static AmazonEC2 EC2 = AmazonEC2ClientBuilder.standard()
            .withRegion(REGION)
            .build();
    private final static String MANAGER_WORKERS_SQS_NAME = "manager-workers";
    private final static String WORKERS_MANAGER_SQS_NAME = "workers-manager";
    private static String workersManagerSqsUrl;
    private static String managerWorkersSqsUrl;
    static ReviewsMap sharedFilessMap = new ReviewsMap();

    public static void main(String[] args) throws Exception {
        System.out.println("image id -" + args[0]);
        String WorkerImage = args[0];
        workersManagerSqsUrl = createSqs(WORKERS_MANAGER_SQS_NAME);
        managerWorkersSqsUrl = createSqs(MANAGER_WORKERS_SQS_NAME);
        System.out.println("created workers-manager queue");
        System.out.println("created manager-workers queue");
        //initialize two threads
        Thread managerWorkersThread = new Thread(new HandleAppMsg(managerWorkersSqsUrl, sharedFilessMap, WorkerImage));
        Thread workersManagerThread = new Thread(new HandleWorkersMsg(workersManagerSqsUrl, sharedFilessMap));
        workersManagerThread.start();
        System.out.println("started workerManagerThread");
        managerWorkersThread.start();
        System.out.println("started managerWorkersThread");
        while (managerWorkersThread.isAlive() && workersManagerThread.isAlive()) {
            workersManagerThread.join();
            workersManagerThread.join();
        }
        System.out.println("threads terminated");
        //delete manager
        DescribeInstancesRequest request = new DescribeInstancesRequest();
        List<String> tagValue = new ArrayList<>();
        tagValue.add("manager");
        DescribeInstancesResult response = EC2.describeInstances(request.withFilters(new Filter().withName("tag:serverType").withValues(tagValue)));
        for (Reservation reservation : response.getReservations()) {
            for (Instance instance : reservation.getInstances()) {
                EC2.terminateInstances(new TerminateInstancesRequest()
                        .withInstanceIds(instance.getInstanceId()));
                System.out.println("delete manager");
            }
        }

    }

    //creates 2 SQS queues, one for msg communication between manager & workers
    //and another one for msg communication between workers & manager
    private static String createSqs(String sqsName) {
        System.out.println("create sqs" + sqsName);

        CreateQueueRequest request = new CreateQueueRequest(sqsName);
        Map<String, String> attributes= new HashMap<String, String>();
        attributes.put("ReceiveMessageWaitTimeSeconds", "20");
        attributes.put("VisibilityTimeout", "120");
        request.withAttributes(attributes);
        try {
            return SQS.createQueue(request).getQueueUrl();
        }catch (com.amazonaws.services.sqs.model.QueueDoesNotExistException e) {
            System.out.println("queue was not created");
            return null;
        }
    }

}

