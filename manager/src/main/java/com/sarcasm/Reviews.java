package com.sarcasm;

public class Reviews {
    private Review[] reviews;
    private String title;

    public Review[] getReviews ()
    {
        return reviews;
    }

    public void setReviews (Review[] reviews)
    {
        this.reviews = reviews;
    }

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }
}
