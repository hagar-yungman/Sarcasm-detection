package com.sarcasm;


import com.amazonaws.AmazonClientException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.*;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.identitymanagement.model.*;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.*;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagement;
import com.amazonaws.services.identitymanagement.AmazonIdentityManagementClientBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;


public class Local {
    private final static String APP_UUID = UUID.randomUUID().toString();
    private final static String BUCKET_NAME = "noahagarsarcasm-" + APP_UUID;
    private final static String INPUT_KEY_NAME = "sarcasm/input/";
    private final static String OUTPUT_KEY_NAME = "sarcasm/output/";
    private final static String APP_MANAGER_SQS_NAME = "app-manager";
    private final static String MANAGER_APP_SQS_NAME = "manager-app-" + APP_UUID;
    public final static Regions REGION = Regions.US_EAST_1;
    private static AmazonS3 S3 = AmazonS3ClientBuilder.standard()
            .withRegion(REGION)
            .build();
    private static AmazonEC2 EC2 = AmazonEC2ClientBuilder.standard()
            .withRegion(REGION)
            .build();
    private static AmazonSQS SQS = AmazonSQSClientBuilder.standard()
            .withRegion(REGION)
            .build();
    private static final AmazonIdentityManagement IAM =
            AmazonIdentityManagementClientBuilder
                    .standard()
                    .withRegion(REGION)
                    .build();
    private static String appManagerSqsUrl;
    private static String managerAppSqsUrl;
    private static HashMap<String, String> inputToOutputFilesMap = new HashMap<>();
    private static int numOfUploadedFiles = 0;


    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        //set app-manager SQS url, create if doesn't exist
        prepareAppManagerQueues();
        //create a unique bucket
        prepareS3BucketForApp();

        // set up all args variables
        String lastArg = args[args.length - 1];
        int index = args.length - 1;
        boolean isTerminate = false;
        if (lastArg.equals("terminate")) {
            isTerminate = true;
            index = args.length - 2;
        }
        int n = Integer.parseInt(args[index]);
        index = index / 2;
        for (int i = 0; i < index; i++) {
            String fileName = args[i];
            inputToOutputFilesMap.put(fileName, args[index + i]);
            //upload input file
            UploadFileToS3(fileName, INPUT_KEY_NAME);
            //add message to SQS
            sendMsg(new MsgToManager(fileName, BUCKET_NAME, INPUT_KEY_NAME, OUTPUT_KEY_NAME, managerAppSqsUrl, false, n));
            numOfUploadedFiles ++;
        }
        //send terminate message
        if (isTerminate) {
            sendMsg(new MsgToManager("", "", "", "", managerAppSqsUrl, true, n));
        }
        //create manager if doesn't exist
        prepareManager();

        //check the SQS for messages (summery file)
        while (true) {
            //get messages from manager
            ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(MANAGER_APP_SQS_NAME);
            List<Message> messages = SQS.receiveMessage(receiveMessageRequest).getMessages();
            for (Message message : messages) {
                ObjectMapper mapper = new ObjectMapper();
                MsgFromManager msgFromManager = null;
                //parse message to review
                try {
                    msgFromManager = mapper.readValue(message.getBody(), MsgFromManager.class);
                } catch (IOException e) {
                    e.printStackTrace();
                    // should never happen
                }
                //download summery
                System.out.println("download file" + msgFromManager.getFileName());
                S3ObjectInputStream file = downloadFileFromS3(msgFromManager.getFileName()).getObjectContent();
                //create html
                System.out.println("create html");
                createHtml(file,inputToOutputFilesMap.get(msgFromManager.getFileName()));
                SQS.deleteMessage(managerAppSqsUrl, message.getReceiptHandle());
                numOfUploadedFiles = numOfUploadedFiles - 1;
                if(numOfUploadedFiles == 0){
                    //delete SQS queues
                    SQS.deleteQueue(new DeleteQueueRequest(MANAGER_APP_SQS_NAME));
                    return;
                }
            }
        }
    }

    private static void createHtml(S3ObjectInputStream file, String fileName) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file));
        String line;
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            while ((line = bufferedReader.readLine()) != null) {
                ObjectMapper mapper1 = new ObjectMapper();
                ReviewResult reviewResult = mapper1.readValue(line, ReviewResult.class);
                Review review = reviewResult.getReview();
                String html = "<h2>" + review.getProductTitle() + "</h2><br>" +
                        "<p style=color: black><b> REVIEW: </b><span style=color:" + reviewResult.getColorSentiment() + ";>" + review.getText() + "</span><br>" +
                        "<b>LINK: </b>" + "<a href=" + review.getLink() + ">" + review.getLink() + "</a></span><br>" +
                        "<b>SENTIMENT: </b>" + String.valueOf(reviewResult.getSentiment()) + "<br>" +
                        "<b>REVIEW: </b>" + String.valueOf(review.getRating()) + "<br>" +
                        "<b>IS-SARCASTIC: </b>:" + String.valueOf(reviewResult.isSarcasm()) + "<br>" +
                        "<b>ENTITIES: </b>:" + reviewResult.getEntities() + "</p><hr>";

                bufferedWriter.write(html);
                bufferedWriter.flush();
            }
            bufferedWriter.close();
            inputToOutputFilesMap.remove(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //------SQS
    private static String getSqsUrl(String sqsName) {
        try {
            return SQS.getQueueUrl(sqsName).getQueueUrl();
        }catch (com.amazonaws.services.sqs.model.QueueDoesNotExistException e) {
            System.out.println("queue was not found");
            return null;
        }
    }

    private static String createSqs(String sqsName) {
        CreateQueueRequest request = new CreateQueueRequest(sqsName);
        Map<String, String> attributes = new HashMap<String, String>();
        attributes.put("ReceiveMessageWaitTimeSeconds", "20");
        attributes.put("VisibilityTimeout", "240");
        request.withAttributes(attributes);
        String result = SQS.createQueue(request).getQueueUrl();
        System.out.println("get sqs: "+ sqsName + "url -------------------------------" +result);
        return result;
    }

    //adds the msg to the queue
    private static void sendMsg(MsgToManager msgToSend) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            SQS.sendMessage(new SendMessageRequest()
                    .withQueueUrl(appManagerSqsUrl)
                    .withMessageBody(mapper.writeValueAsString(msgToSend)));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private static void prepareAppManagerQueues() {
        appManagerSqsUrl = getSqsUrl(APP_MANAGER_SQS_NAME);
        if (appManagerSqsUrl == null) {
            appManagerSqsUrl = createSqs(APP_MANAGER_SQS_NAME);
        }
        //create manager-app SQS
        managerAppSqsUrl = createSqs(MANAGER_APP_SQS_NAME);
    }

    //------EC2
    //check if manager is active
    private static Instance getManagerInstance() {
        DescribeInstancesRequest request = new DescribeInstancesRequest();
        List<String> tagValue = new ArrayList<>();
        tagValue.add("manager");
        DescribeInstancesResult response = EC2.describeInstances(request.withFilters(new Filter().withName("tag:serverType").withValues(tagValue)));
        Instance managerInstance = null;
        for (Reservation reservation : response.getReservations()) {
            for (Instance instance : reservation.getInstances()) {
                if (instance.getState().getName().equals((String) "stopped")) {
                    //manager is in stopped state, activate it
                    managerInstance = instance;
                    EC2.startInstances(new StartInstancesRequest()
                            .withInstanceIds(instance.getInstanceId()));
                    break;
                } else if (instance.getState().getName().equals((String) "running") || instance.getState().getName().equals((String) "pending")) {
                    managerInstance = instance;
                    break;
                }
            }
        }
        return managerInstance;
    }

    private static RunInstancesResult createNewInstance() {
        String imageId = "ami-817731fb";
        String base64UserData = "";
        String userData = "#!/bin/bash \n" +
                "set -x \n" +
                "java -jar /home/ec2-user/manager.jar " + imageId + "\n";
        try {
            base64UserData = new String(Base64.encodeBase64(userData.getBytes("UTF-8")), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        TagSpecification tagSpecification = new TagSpecification().withResourceType("instance")
                .withTags(new Tag("serverType", "manager"));
        RunInstancesRequest run_request = new RunInstancesRequest()
                .withImageId(imageId).withUserData(base64UserData)
                .withInstanceType(InstanceType.T2Micro)
                .withMaxCount(1)
                .withMinCount(1)
                .withTagSpecifications(tagSpecification)
                .withIamInstanceProfile(new IamInstanceProfileSpecification().withName("ManagerRole"));

        return EC2.runInstances(run_request);
    }

    private static void createRole(String roleName, String policyName, String policyFileLocation) throws IOException {
        //check if role exist
        try {
            GetRoleResult role = IAM.getRole(new GetRoleRequest().withRoleName(roleName));
            System.out.println("role already exist" + role.toString());
        }
        //if role doesn't exist create the role
        catch (com.amazonaws.services.identitymanagement.model.AmazonIdentityManagementException e) {
            System.out.println("create role");
            CreateRoleRequest createRoleRequest = new CreateRoleRequest()
                    .withRoleName(roleName)
                    .withAssumeRolePolicyDocument(new String(Files.readAllBytes(Paths.get("AssumeEc2Role.json"))));
            PutRolePolicyRequest putRolePolicyRequest =
                    new PutRolePolicyRequest()
                            .withRoleName(roleName)
                            .withPolicyDocument(new String(Files.readAllBytes(Paths.get(policyFileLocation))))
                            .withPolicyName(policyName);
            IAM.createRole(createRoleRequest);
            IAM.putRolePolicy(putRolePolicyRequest);
            CreateInstanceProfileRequest createInstanceProfileRequest = new CreateInstanceProfileRequest()
                    .withInstanceProfileName(roleName);
            IAM.createInstanceProfile(createInstanceProfileRequest);
            AddRoleToInstanceProfileRequest addRoleToInstanceProfileRequest = new AddRoleToInstanceProfileRequest()
                    .withInstanceProfileName(roleName)
                    .withRoleName(roleName);
            IAM.addRoleToInstanceProfile(addRoleToInstanceProfileRequest);
        }
    }

    private static void prepareManager() throws IOException {
        //create IAM role for manager and worker
        createRole("ManagerRole", "ManagerPolicy", "ManagerPolicy.json");
        createRole("WorkerRole", "WorkerPolicy", "WorkerPolicy.json");

        //check if a manager exists
        Instance managerInstance = getManagerInstance();
        if (managerInstance == null) {
            //create manager
            System.out.println("craete manager");
            createNewInstance();
        }
        else{
            System.out.println("manager already exist");
        }
    }

    //------S3
    private static void prepareS3BucketForApp() {
        try {
            System.out.println("create S3" + BUCKET_NAME);
            S3.createBucket(new CreateBucketRequest(
                    BUCKET_NAME));
        } catch (AmazonS3Exception e) {
            System.err.println(e.getErrorMessage());
        }

        //open two folders for input files and output files
        UploadFileToS3("noa.txt", OUTPUT_KEY_NAME);
        UploadFileToS3("noa.txt", INPUT_KEY_NAME);
    }

    private static void UploadFileToS3(String inputFile, String path) {
        try {
            System.out.println("Uploading a new object to S3 from a file\n");
            File file = new File(inputFile);
            S3.putObject(new PutObjectRequest(
                    BUCKET_NAME, path + file.getName(), file));
        } catch (AmazonClientException e) {
            e.printStackTrace();
        }
    }

    private static S3Object downloadFileFromS3(String fileName) {
        try {
            System.out.println("Downloading an object");
            return S3.getObject(new GetObjectRequest(BUCKET_NAME, OUTPUT_KEY_NAME + fileName));
        } catch (AmazonClientException e) {
            e.printStackTrace();

        }
        return null;
    }


}
