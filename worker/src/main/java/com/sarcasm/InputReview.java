package com.sarcasm;

public class InputReview {

    private String bucketName;
    private String outputKeyName;
    //TODO delete
    private String appSqsUrl;
    private String fileName;
    private String productTitle;
    private String id;
    private String link;
    private String title;
    private String text;
    private Integer rating;
    private String author;
    private String date;

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getOutputKeyName() {
        return outputKeyName;
    }

    public void setOutputKeyName(String outputKeyName) {
        this.outputKeyName = outputKeyName;
    }

    public String getAppSqsUrl() {
        return appSqsUrl;
    }

    public void setAppSqsUrl(String appSqsUrl) {
        this.appSqsUrl = appSqsUrl;
    }

    public void setFileName(String _fileNumber){
        fileName = _fileNumber;
    }

    public void setProductTitle(String _productTitle){
        productTitle = _productTitle;
    }

    public String getFileName(){
        return fileName;
    }

    public String getProductTitle(){
        return productTitle;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
